package tgfmt

import "testing"

func TestBold(t *testing.T) {
	want := `This is a <b>bold</b> text.`
	got := Sprintf("This is a $b%s text.", "bold")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestFixed(t *testing.T) {
	want := `This is a <pre>fixed</pre> text.`
	got := Sprintf("This is a $f%s text.", "fixed")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestItalic(t *testing.T) {
	want := `This is a <i>italic</i> text.`
	got := Sprintf("This is a $i%s text.", "italic")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestUnderline(t *testing.T) {
	want := `This is a <u>underline</u> text.`
	got := Sprintf("This is a $u%s text.", "underline")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestStrikethrough(t *testing.T) {
	want := `This is a <strikethrough>strikethrough</strikethrough> text.`
	got := Sprintf("This is a $d%s text.", "strikethrough")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestSpoiler(t *testing.T) {
	want := `This is a <tg-spoiler>spoiler</tg-spoiler> text.`
	got := Sprintf("This is a $s%s text.", "spoiler")

	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestLink(t *testing.T) {
	want := `This is a <a href="https://example.com">super link</a> text.`
	got := Sprintf("This is a $n%s text.", Link("super link", "https://example.com"))
	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}

func TestBoldLink(t *testing.T) {
	want := `This is a <b><a href="https://example.com">bold super link</a></b> text.`
	got := Sprintf("This is a $nb%s text.", Link("bold super link", "https://example.com"))
	if got != want {
		t.Errorf("unexpected output, want: %s, got: %s", want, got)
	}
}
