package tgfmt

import (
	"html"
)

func Link(display, url string) string {
	return Sprintf(`<a href="%s">%s</a>`, url, html.EscapeString(display))
}
